/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$.ajaxSetup({
   async: false
 });
var slides = [];
var position = 0;
var show;

function main(){
    var title;
    $.getJSON("./js/myJSON.json", function(data){
        title = data.title;
        $.each(data.slides, function(index, object) {
            slides.push(object);
        });
    });
    $("#header").text(title);
    updateSlide(position);
    $("#previous").click(function(){
        prevSlide();
    });
    $("#next").click(function(){
        nextSlide();
    });
    $("#playpause").on('click', play);
};

function play() {
    show = setInterval(nextSlide, 3000);
    $("#playpause").attr("src", "imgs/pause.png");
    $("#playpause").off('click').on('click', pause);
}
function pause() {
    clearInterval(show);
    $("#playpause").attr("src", "imgs/Play.png");
    $("#playpause").off('click').on('click', play);
}
function prevSlide(){
        position--;
        if (position < 0)
            position = slides.length - 1;
        updateSlide(position);
};
function nextSlide(){
    position++;
    if (position === slides.length)
        position = 0;
    updateSlide(position);
};

function updateSlide(position){
    var text = slides[position].text;
    $("#caption").text(text);
    $("#imageDiv").children("img").remove();
    var img = document.createElement("img");
    img.src =  "./imgs/" + slides[position].image_file_name;
    document.getElementById('imageDiv').appendChild(img);
};

$(document).ready(main);

