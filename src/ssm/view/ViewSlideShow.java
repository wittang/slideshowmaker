/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.PATH_SITES;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.file.SlideShowFileManager.JSON_EXT;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 *
 * @author Wilson Tang
 */
/*
public class ViewSlideShow {
    
    SlideShowModel slideShow;
    Stage view;
    Scene viewScene;
    VBox viewVBox;
    Label viewTitle;
    ImageView slideView;
    HBox buttonPane;
    Label viewCaption;
    Button prevSlide;
    Button nextSlide;
    int index;
    public ViewSlideShow(SlideShowModel slideShowToView){
        slideShow = slideShowToView;
    }
    
    public void initView(){
        view = new Stage();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(TITLE_WINDOW);
        view.setTitle(title);
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        // AND USE IT TO SIZE THE WINDOW
        view.setX(bounds.getMinX());
	view.setY(bounds.getMinY());
	view.setWidth(bounds.getWidth());
	view.setHeight(bounds.getHeight());
        //create vbox in scene;
        viewVBox = new VBox(20);
        viewVBox.setAlignment(Pos.CENTER);
        //create items in vbox
        viewTitle = new Label(slideShow.getTitle());
        slideView = new ImageView();
        slideView.setFitHeight(570);
        slideView.setFitWidth(810);
        updateViewImage(0);
        viewCaption = new Label(slideShow.getSlides().get(index).getText());
        buttonPane = new HBox(50);
        buttonPane.setAlignment(Pos.CENTER);
        prevSlide = initButton(buttonPane, ICON_PREVIOUS, TOOLTIP_PREVIOUS_SLIDE);
        nextSlide = initButton(buttonPane, ICON_NEXT, TOOLTIP_NEXT_SLIDE);
        prevSlide.setDisable(true);
        initViewHandlers();
        viewVBox.getChildren().addAll(viewTitle, slideView, viewCaption, buttonPane);
        viewScene = new Scene(viewVBox);
        view.setScene(viewScene);
        view.show();
    }
    public void updateViewImage(int index){
        String imagePath = slideShow.getSlides().get(index).getImagePath() + SLASH + slideShow.getSlides().get(index).getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    slideView.setImage(slideImage);
	} catch (Exception e) {
	    //handled in slideEditView
	}
    }
    public Button initButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    public void initViewHandlers(){
        if (index == slideShow.getSlides().size()-1)
            nextSlide.setDisable(true);
        nextSlide.setOnAction(e->{
            prevSlide.setDisable(false);
            prevSlide.setOnAction(d->{
                nextSlide.setDisable(false);
                updateViewImage(index-1);
                viewCaption.setText(slideShow.getSlides().get(index-1).getText());
                index--;
                if (index == 0)
                    prevSlide.setDisable(true);
            });
            updateViewImage(index+1);
            viewCaption.setText(slideShow.getSlides().get(index+1).getText());
            index++;
            if (index == slideShow.getSlides().size()-1)
                nextSlide.setDisable(true);
        });
    }
}
*/
public class ViewSlideShow {
    SlideShowMakerView slideShowView;
    Stage stage;
    WebView view;
    Scene viewScene;
    public ViewSlideShow(SlideShowMakerView slideShowView){
        this.slideShowView = slideShowView;
    }
    public void initView(){
        try{
        initViewHandlers();
        } catch (Exception e){
            System.out.println("error");
        }
        stage = new Stage();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(TITLE_WINDOW);
        stage.setTitle(title);
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        // AND USE IT TO SIZE THE WINDOW
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
	stage.setWidth(bounds.getWidth());
	stage.setHeight(bounds.getHeight());
        view = new WebView();
        String htmlSource = PATH_SITES + slideShowView.getSlideShow().getTitle() + "/index.html";
        File htmlFile = new File(htmlSource);
        view.getEngine().load(htmlFile.toURI().toString());
        viewScene = new Scene(view);
        stage.setScene(viewScene);
        stage.show();
        
    }
    public void initViewHandlers() throws IOException{
        slideShowView.getSSFM().saveSlideShow(slideShowView.getSlideShow());
        String slideShowTitle = slideShowView.getSlideShow().getTitle();
        //create folders
        File newSite = new File(PATH_SITES + slideShowTitle);
        newSite.mkdir();
        
        //copy base html into new site folder
        File baseHTML = new File(PATH_SITES + "base/index.html");
        File newHTML = new File(newSite.toPath().toString() + SLASH + baseHTML.getName());
        Files.copy(baseHTML.toPath(), newHTML.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        //create CSS folder and copy base CSS into CSS folder
        File cssFolder = new File(newSite.toPath().toString() + "/css");
        cssFolder.mkdir();
        File baseCSS = new File(PATH_SITES + "base/css/SlideShowMaker.css");
        File newCSS = new File(cssFolder.toPath().toString() + SLASH + baseCSS.getName());
        Files.copy(baseCSS.toPath(), newCSS.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        //create imgs folder and copy slide show images from path to imgs folder
        File imgsFolder = new File(newSite.toPath().toString() + "/imgs");
        imgsFolder.mkdir();
        File baseIMG = new File(PATH_SITES + "base/imgs");
        File[] baseIMGs = baseIMG.listFiles();
        for (File baseIMG1 : baseIMGs) {
            File baseImage = new File(imgsFolder.toPath().toString() + "/" + baseIMG1.getName());
            Files.copy(baseIMG1.toPath(), baseImage.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        for (int i = 0; i < slideShowView.getSlideShow().getSlides().size(); i++){
            String imgPath = slideShowView.getSlideShow().getSlides().get(i).getImagePath() +
                    slideShowView.getSlideShow().getSlides().get(i).getImageFileName();
            File imageToCopy = new File(imgPath);
            File image = new File(imgsFolder.toPath().toString() + SLASH + imageToCopy.getName());
            Files.copy(imageToCopy.toPath(), image.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        
        //create js folder and copy base js to js folder
        File jsFolder = new File(newSite.toPath().toString() +  "/js");
        jsFolder.mkdir();
        File baseJS = new File(PATH_SITES + "base/js/SlideShowMaker.js");
        File newJS = new File(jsFolder.toPath().toString() + SLASH + baseJS.getName());
        Files.copy(baseJS.toPath(), newJS.toPath(), StandardCopyOption.REPLACE_EXISTING);
        File baseJQuery = new File(PATH_SITES + "base/js/jquery-1.11.3.min.js");
        File newJQuery = new File(jsFolder.toPath().toString() + SLASH + baseJQuery.getName());
        Files.copy(baseJQuery.toPath(), newJQuery.toPath(), StandardCopyOption.REPLACE_EXISTING);
        File jsonToCopy = new File(PATH_SLIDE_SHOWS + SLASH + slideShowTitle + JSON_EXT);
        File newJson = new File(jsFolder.toPath().toString() + SLASH + "myJSON.json");
        Files.copy(jsonToCopy.toPath(), newJson.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }
}

    

