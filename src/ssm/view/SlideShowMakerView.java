package ssm.view;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.*;
import ssm.StartupConstants;
import static ssm.StartupConstants.*;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & Wilson Tang
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;
    
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    
    // WORKSPACE
    HBox workspace;
    BorderPane displaySS;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button changeTitleButton;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveUpSlideButton;
    Button moveDownSlideButton;
    
    //universal objects
    Stage stage;
    Scene scene;
    VBox pane;
    Button okButton;
    Label label;
    
    //title objects
    TextField titleTextField;
    String slideShowTitle;

    //Save Prompt objects
    HBox buttonPane;
    Button yesButton;
    Button noButton;
    Button cancelButton;
    String saved;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;
    ViewSlideShow slideView;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
        //initProper
        
        //THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }
    public void initTitle(String message){
        stage = new Stage();
        pane = new VBox(10);
        scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        titleTextField = new TextField();
        okButton = new Button("OK");
        titleTextField.setOnAction(e->{
            slideShowTitle = titleTextField.getText();
            slideShow.setTitle(slideShowTitle);
            initTitle2(message);
            stage.setScene(scene);
        });
        okButton.setOnAction(e ->   {
            slideShowTitle = titleTextField.getText();
            slideShow.setTitle(slideShowTitle);
            initTitle2(message);
            stage.setScene(scene);
        });
        pane.getChildren().addAll(titleTextField, okButton);
        pane.getStyleClass().add(StartupConstants.CSS_CLASS_PROMPTS);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(DEFAULT_SLIDE_SHOW_TITLE);
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }
    public void initTitle2(String message){
        pane = new VBox(10);
        scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        Label titleLabel= new Label(message);
        okButton = new Button("OK");
        okButton.setOnAction(e ->
            stage.close()
        );
        pane.getChildren().addAll(titleLabel, okButton);
        pane.getStyleClass().add(StartupConstants.CSS_CLASS_PROMPTS);
    }
    
    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
        changeTitleButton = this.initChildButton(slideEditToolbar, ICON_CHANGE_TITLE, TOOLTIP_CHANGE_TITLE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
	addSlideButton = this.initChildButton(slideEditToolbar,	ICON_ADD_SLIDE, TOOLTIP_ADD_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
	removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveUpSlideButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, TOOLTIP_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveDownSlideButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        slidesEditorScrollPane.getStyleClass().add(CSS_CLASS_SLIDEVBOX);
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
        workspace.getStyleClass().add(CSS_CLASS_WORKSPACE);
    }
    
    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
            fileController.handleNewSlideShowRequest();
            reloadSlideShowPane(slideShow);
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
            slideView = new ViewSlideShow(this);
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
        viewSlideShowButton.setOnAction(e->{
            slideView.initView();
        });
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
        changeTitleButton.setOnAction(e->{
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            initTitle(props.getProperty(SLIDE_SHOW_RENAMED));
            fileController.markAsEdited();
        });
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
            slideView = new ViewSlideShow(this);
            fileController.markAsEdited();
	});
	removeSlideButton.setOnAction(e -> {
	    editController.processRemoveSlideRequest();
            fileController.markAsEdited();
        });
	moveUpSlideButton.setOnAction(e -> {
	    editController.processMoveUpRequest();
            fileController.markAsEdited();
            if (slideShow.getSlides().indexOf(slideShow.getSelectedSlide()) == 0)
                moveUpSlideButton.setDisable(true);
        });
	moveDownSlideButton.setOnAction(e -> {
	    editController.processMoveDownRequest();
            fileController.markAsEdited();
            if (slideShow.getSlides().indexOf(slideShow.getSelectedSlide()) == slideShow.getSlides().size()-1)
                moveDownSlideButton.setDisable(true);
        });
    }
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW, TOOLTIP_NEW_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW, TOOLTIP_LOAD_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW, TOOLTIP_SAVE_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW, TOOLTIP_VIEW_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        fileToolbarPane.getStyleClass().add(CSS_CLASS_FLOWPANE);
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved, SlideShowModel model) {
        boolean update = false;
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
        //update view slide button
        if (model.getSlides().isEmpty())
            update = true;
        viewSlideShowButton.setDisable(update);
        //update slide edit buttons
        if (!model.isSlideSelected())
            update = true;
        removeSlideButton.setDisable(update);
        moveUpSlideButton.setDisable(update);
        moveDownSlideButton.setDisable(update);
    }
    /*public void updateSaveSlideButton(boolean saved){
        saveSlideShowButton.setDisable(saved);
    }*/
    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide);
            //SlideEditView HBox caption textfield event handler
            slideEditor.captionTextField.setOnKeyReleased(e->{
                slide.setText(slideEditor.captionTextField.getText());
                saveSlideShowButton.setDisable(false);
            });
            //SlideEditView HBox mouse clicked event handler
            slideEditor.setOnMouseClicked(e->{
                //do this when no slide is selected
                if (!slideShowToLoad.isSlideSelected()){
                    slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW_SELECTED);
                    slideShowToLoad.setSelectedSlide(slide);
                    slideShowToLoad.setIndex(slideShowToLoad.getSlides().indexOf(slideShowToLoad.getSelectedSlide()));
                    updateToolbarControls(false, slideShowToLoad);
                //enable/disable up/down buttons top or bottom slides are selected
                if (slideShowToLoad.getSlides().indexOf(slideShowToLoad.getSelectedSlide()) == 0)
                    moveUpSlideButton.setDisable(true);
                if (slideShowToLoad.getSlides().indexOf(slideShowToLoad.getSelectedSlide()) == slideShow.getSlides().size()-1)
                    moveDownSlideButton.setDisable(true);
                }
                //do this when a slide is already selected
                else if (slideShowToLoad.isSlideSelected()){
                    ((SlideEditView)slidesEditorPane.getChildren().get(slideShowToLoad.getIndex())).getStyleClass().remove(CSS_CLASS_SLIDE_EDIT_VIEW_SELECTED);
                    slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW_SELECTED);
                    slideShowToLoad.setSelectedSlide(slide);
                    slideShowToLoad.setIndex(slideShowToLoad.getSlides().indexOf(slideShowToLoad.getSelectedSlide()));
                    updateToolbarControls(false, slideShowToLoad);
                //enable/disable up/down buttons top or bottom slides are selected
                if (slideShowToLoad.getSlides().indexOf(slideShowToLoad.getSelectedSlide()) == 0)
                    moveUpSlideButton.setDisable(true);
                if (slideShowToLoad.getSlides().indexOf(slideShowToLoad.getSelectedSlide()) == slideShow.getSlides().size()-1)
                    moveDownSlideButton.setDisable(true);
                }
            });
            if (slide == slideShowToLoad.getSelectedSlide())
                slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW_SELECTED);
            //add saved text back into caption TextField
            slideEditor.captionTextField.setText(slide.getText());
            //add SlideEditView's into pane
            slidesEditorPane.getChildren().add(slideEditor);
        }
    }  
    public void initSave(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        stage = new Stage();
        stage.setTitle(props.getProperty(SAVE_SLIDESHOW));
        label = new Label(props.getProperty(SAVE_QUESTION));
        pane = new VBox(10);
        pane.getStyleClass().add(StartupConstants.CSS_CLASS_PROMPTS);
        scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        yesButton = new Button("Yes");
        noButton = new Button("No");
        cancelButton = new Button("Cancel");
        yesButton.setOnAction(e->{
            saved = "save";
            stage.close();
        });
        noButton.setOnAction(e->{
            saved = "dont save";
            stage.close();
        });
        cancelButton.setOnAction(e->{
            saved = "";
            stage.close();
        });
        buttonPane = new HBox(10);
        buttonPane.setAlignment(Pos.CENTER);
        buttonPane.getChildren().addAll(yesButton, noButton, cancelButton);
        pane.getChildren().addAll(label, buttonPane);
        stage.setScene(scene);
        stage.showAndWait();
    }
    public String getSave(){
        return saved;
    }
    public void errorPrompt(String errorTitle, String errorMessage){
        stage = new Stage();
        stage.setTitle(errorTitle);
        pane = new VBox(10);
        scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        label = new Label(errorMessage);
        label.setWrapText(true);
        okButton = new Button("OK");
        okButton.setOnAction(e -> {
            stage.close();
        });
        pane.getChildren().addAll(label, okButton);
        pane.getStyleClass().add(StartupConstants.CSS_CLASS_PROMPTS);
        stage.setScene(scene);
        stage.showAndWait();
    } 
    public SlideShowFileManager getSSFM(){
        return fileManager;
    }
}
    

    
