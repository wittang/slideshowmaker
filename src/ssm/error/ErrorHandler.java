package ssm.error;

import ssm.view.SlideShowMakerView;

/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 * @author McKilla Gorilla & Wilson Tang
 */
public class ErrorHandler {
    // APP UI
    private SlideShowMakerView ui;
    
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
    }
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     */
    public void processError(String errorTitle, String errorMessage){
        ui.errorPrompt(errorTitle, errorMessage);
    }    
    
}
