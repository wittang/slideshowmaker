package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & Wilson Tang
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    int selectedSlideIndex;
    
    public SlideShowModel(SlideShowMakerView initUI) {
        title = "No Title Set";
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    public int getIndex(){
        return selectedSlideIndex;
        //return slides.indexOf(selectedSlide);
    }
    public void setIndex(int index){
        selectedSlideIndex = index;
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	title = getTitle();
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath, String initText) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath);
        slideToAdd.setText(initText);
	slides.add(slideToAdd);
	ui.reloadSlideShowPane(this);
    }
}