package ssm;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.*;
import static ssm.StartupConstants.APP_ICON;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_SP;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & Wilson Tang
 */
public class SlideShowMaker extends Application {
    String choice;
    boolean success;
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    final SlideShowMakerView ui = new SlideShowMakerView(fileManager);

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox comboPane = new VBox(15);
        Scene propScene = new Scene(comboPane);
        propScene.getStylesheets().add(STYLE_SHEET_UI);
        comboPane.getStyleClass().add(StartupConstants.CSS_CLASS_PROMPTS);
        Label propLabel = new Label("Choose Language:");
        ComboBox propComboBox = new ComboBox();
        propComboBox.getItems().addAll("English", "Spanish");
        Button okButton = new Button("OK");
        okButton.setDisable(true);
        propComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (propComboBox.getValue() == "English"){
                    choice = UI_PROPERTIES_FILE_NAME;
                    okButton.setDisable(false);
                }
                else if (propComboBox.getValue() == "Spanish"){
                    choice = UI_PROPERTIES_FILE_NAME_SP;
                    okButton.setDisable(false);
                }
            }
        });
        comboPane.getChildren().addAll(propLabel ,propComboBox, okButton);
        primaryStage.setTitle("Language Preference");
        primaryStage.setScene(propScene);
        primaryStage.getIcons().add(new Image("file:" + PATH_ICONS + APP_ICON));
        okButton.setOnAction(e -> {
            success = loadProperties(choice);
            if (success) {
            // LOAD APP SETTINGS INTO THE GUI AND START IT UP
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String appTitle = props.getProperty(TITLE_WINDOW);
                // NOW START THE UI IN EVENT HANDLING MODE
                ui.startUI(primaryStage, appTitle);
            }// THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
            else {
            //handled in load properties method
                System.exit(0);
            }
        }); 
        primaryStage.show();
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties(String languageChoice) {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(languageChoice, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            ErrorHandler errorHandler = ui.getErrorHandler();
            errorHandler.processError("File Error", "Error loading application properties.");
            return false;
        }        
    }
    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
}
