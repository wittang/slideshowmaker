package ssm.controller;

import properties_manager.PropertiesManager;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & Wilson Tang
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES, "");
        ui.updateToolbarControls(false, slideShow);
    }
    public void processRemoveSlideRequest(){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.getSlides().remove(slideShow.getSelectedSlide());
        slideShow.setSelectedSlide(null);
        //slideShow.setIndex(0);
        ui.updateToolbarControls(false, slideShow);
        ui.reloadSlideShowPane(slideShow);
    }
    public void processMoveUpRequest(){
        Slide slide;
        SlideShowModel slideShow = ui.getSlideShow();
        if (slideShow.getIndex() == 0){}
        else {
            slide = slideShow.getSelectedSlide();
            slideShow.getSlides().set(slideShow.getIndex(), slideShow.getSlides().get(slideShow.getIndex()-1));
            slideShow.getSlides().set(slideShow.getIndex()-1, slide);
            slideShow.setIndex(slideShow.getSlides().indexOf(slide));
            ui.reloadSlideShowPane(slideShow);
            ui.updateToolbarControls(false, slideShow);
        }
    }
    public void processMoveDownRequest(){
        Slide slide;
        SlideShowModel slideShow = ui.getSlideShow();
        if (slideShow.getIndex() == (slideShow.getSlides().size()-1)){}
        else {
            slide = slideShow.getSelectedSlide();
            slideShow.getSlides().set(slideShow.getIndex(), slideShow.getSlides().get(slideShow.getIndex()+1));
            slideShow.getSlides().set(slideShow.getIndex()+1, slide);
            slideShow.setIndex(slideShow.getSlides().indexOf(slide));
            ui.reloadSlideShowPane(slideShow);
            ui.updateToolbarControls(false, slideShow);
        }
    }
}
